/* eslint-env mocha */
'use strict'

const Formatter = require('../src/formatter.js')
const sinon = require('sinon')
const assert = require('assert')
const htrresp = require('./fixture/package-hunter-response.json')
const dependencyScanningSample = require('./fixture/gitlab-dependency-scanning-sample.json')
// const { v4: uuidv4 } = require('uuid')
const uuid = require('uuid')

describe('Formatter Test', function () {
  describe('ctor', function () {
    it('sets default for output format', function () {
      const f = new Formatter(process.stdout)
      assert(f.format === Formatter.HUNTER)
    })

    it('sets default for uuidv4', function () {
      const f = new Formatter(process.stdout)
      assert(f.uuidv4 === uuid.v4)
    })

    it('throws if not invoked with writeable stream', function () {
      function shouldThrow () {
        const noStream = {}
        new Formatter(noStream)
      }

      assert.throws(shouldThrow, TypeError, 'first parameter expected to be a writeable stream')
    })
  })

  describe('print', function () {
    beforeEach(function () {
      this.api = { write: function () { console.log('write called') } }
      this.mockStdout = sinon.mock(this.api)
    })

    afterEach(function () {
      this.mockStdout.restore()
    })

    it('prints Hunter format', function () {
      const f = new Formatter(this.api, Formatter.HUNTER)
      this.mockStdout.expects('write').once().withArgs(sinon.match(function (value) {
        assert.deepEqual(JSON.parse(value), htrresp)
        return true
      }))

      f.print(htrresp)
      this.mockStdout.verify()
    })

    it('prints GitLab format', function () {
      const func = () => 'some-uuid-v4-1234'
      const f = new Formatter(this.api, Formatter.GITLAB, func)
      this.mockStdout.expects('write').once().withArgs(sinon.match(function (value) {
        assert.deepEqual(JSON.parse(value), dependencyScanningSample)
        return true
      }))

      f.print(htrresp)
      const expected = f._jsonToString(dependencyScanningSample)

      // console.log(this.mockStdout.expects('write').firstCall)

      this.mockStdout.verify()
    })
  })
})
